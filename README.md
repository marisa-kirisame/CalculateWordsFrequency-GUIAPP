# Python进行关键字词统计GUI软件

#### 介绍
使用Python写的对TXT文本文件中指定关键字词进行统计的软件，为方便使用，设计了GUI界面进行控制。


#### 安装

1.  Python
2.  tkinter库
3.  jieba库

#### 使用说明

1.  在Python的IDE中打开，run即可
2.  需要选择文件
